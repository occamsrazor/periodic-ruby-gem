# PeriodicRuby

The Periodic.is gem provides easy communication between a ruby/rails application and the Periodic API.

## Installation

To test, clone the repository and then execute

$ gem build periodic_ruby.gemspec

$ gem install periodic_ruby

$ irb

$ require 'periodic_ruby'

These commands will install the ruby gem locally, then open an interactive ruby display and include the periodic_ruby gem in your environment.

## Usage

First, setup the API by calling it with your username, api key, domain, and optionally a provider subdomain. For rails applications, this could be placed in an initializer.

$ api = PeriodicRuby::API.new("username","apikey","periodicdev.com")

At this point, you can access all objects through the standard methods (index, get, create, update, destroy) with the following syntax:

### Create a new provider (returns the hash of the provider)

provider = api.providers.create(:name=>"a new provider", :firstname=>"first", :lastname=>"last", :subdomain=>"new-subdomain", :email=>"newemail@example.com")

### Get a provider (returns the hash of the provider)

api.providers.get(:id=>provider["id"])

### Update a provider (returns the hash of the provider)

api.providers.update(:id=>provider["id"],:firstname=>"new first name")

### List all providers (returns an array of hashes of all providers)

api.providers.index #will return an array of all resources

### Destroy a provider (returns true on success)

api.providers.destroy(:id=>provider["id"])

## Using a provider's subdomain when initializing the API
    
When the subdomain is provided, the api can easily access all objects in the same way, except in any case where the provider subdomain is a required field for an object (such as bookables), it is automatically entered for them and is not needed as a parameter.  For example:

### Creating a new blackout

api = PeriodicRuby::API.new("username","apikey","periodicdev.com","provider-subdomain")

blackout = api.blackouts.create(:start=>Date.today,:end=>Date.today+5)

This will create a new blackout under provider-subdomain's account.

### Update a blackout
  
api.blackouts.update(:end=>Date.today+10)

### Delete a blackout

api.blackouts.destroy(:id=>blackout["id"])

And so on for all other object types. All objects and their required fields can be reviewed at periodic.is/api.