# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'periodic_ruby/version'

Gem::Specification.new do |spec|
  spec.name          = "periodic_ruby"
  spec.version       = PeriodicRuby::VERSION
  spec.authors       = ["Anthony Leon"]
  spec.email         = ["anthony@osirisdevelopers.com"]
  spec.summary       = %q{Periodic.is API Ruby Gem}
  spec.description   = %q{Periodic.is API Ruby Gem -- long description}
  spec.homepage      = "http://www.periodic.is"
  spec.license       = "MIT"

  spec.files         = Dir['lib/**/*.rb']
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_dependency 'json', '>= 1.7.7'
  spec.add_dependency 'excon', '>= 0.16.0'
end
