require "json"
require "excon"
require 'hmac-md5'
require 'date'

require "periodic_ruby/version"
require "periodic_ruby/errors"
require "periodic_ruby/api"

module PeriodicRuby
  class API
    
    attr_accessor :host, :username, :apikey, :domain, :subdomain, :session, :protocol

    def initialize(username=nil, apikey=nil, domain=nil, subdomain=nil, protocol="http")
      unless username
        apikey = ENV['PERIODIC_IS_USERNAME']
      end

      raise Error, "You must provide a username" unless apikey 

      unless apikey 
        apikey = ENV['PERIODIC_IS_APIKEY']
      end

      raise Error, "You must provide an API key" unless apikey 

      unless domain
        domain = ENV['PERIODIC_IS_DOMAIN']
      end

      raise Error, "You must provide a marketplace domain" unless domain

      @username = username
      @apikey = apikey
      @protocol = protocol=="http" ? "http://" : "#{protocol}://"
      @domain = domain 
      @subdomain = subdomain

      @session = generate_session

    end

    def call(url,verb,params={})
      body = JSON.generate(params)
      session = generate_session(params)
      request = session.request(:method=>verb, :path => "#{url}", :headers => authentication_header(url,verb,params), :body=>body)
      raise Error, "An error was encountered: #{request.body}" if request.status != 200
      begin 
        return JSON.parse(request.body)
      rescue
        return true
      end
    end

    def generate_session(params={})
      if params[:provider]
        Excon.new("#{@protocol}#{params[:provider]}.#{@domain}")
      elsif @subdomain
        Excon.new("#{@protocol}#{@subdomain}.#{domain}")
      else
        Excon.new("#{@protocol}#{@domain}")
      end
    end

    def providers
      Providers.new self
    end

    def bookables
      Bookables.new self
    end

    def resources
      Resources.new self
    end

    def blackouts
      Blackouts.new self
    end

    def timeslots
      Timeslots.new self
    end

    def reservations
      Reservations.new self
    end

    def users
      Users.new self
    end

    def bookable_tags
      BookableTags.new self 
    end

    def requirement_groups
      RequirementGroups.new self 
    end

    def bookable_questions
      BookableQuestions.new self 
    end

    def tickets
      Tickets.new self 
    end

    private 
      def authentication_header(url,verb,params)
        headers = {'Content-Type' => 'application/json'}
        hash = HMAC::MD5.new @apikey
        #if delete/get, hash URI. If post/put, hash body
        (["delete","get"].include?(verb)) ? hash.update(url) : hash.update(serialize_hash(params))
        hash_value = hash.hexdigest
        headers['WWW-Authenticate'] = "#{@username}::#{hash_value}"
        headers['x-periodic-provider'] = params[:provider] if params[:provider]
        return headers
      end

      def convert_object_to_string(obj)
        case "#{obj.class}"
        when "Array"
          obj.map { |item| convert_object_to_string(item) }.join(",")
        when "Hash"
          obj.sort.map {|key,value| "#{key.to_s}#{convert_object_to_string(value)}"}.join("")
        else
          obj.to_s
        end
      end

      def serialize_hash(params)
        params.sort.map {|k,v| "#{k.to_s}#{convert_object_to_string(v)}"}.join("")
      end

  end
end
