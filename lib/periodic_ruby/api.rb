module PeriodicRuby
  class StandardCalls
    attr_accessor :endpoint

    def initialize(master,endpoint)
      @master = master 
      @endpoint = endpoint 
    end 

    def get(params)
      id = params[:id]
      params = process_dates(params)
      raise Error, "Please provide an ID" unless id
      @master.call "/#{@endpoint}/id/#{id}", "get", params
    end

    def index(params={})
      @master.call "/#{@endpoint}/index/", "get", params
    end

    def search(params)
      params = process_dates(params)
      search = serialize_params(params)
      @master.call "/#{@endpoint}/search/#{search}/", "get", params
    end

    def create(params)
      params = process_dates(params)
      @master.call "/#{@endpoint}/", "post", params
    end

    def update(params)
      id = params[:id]
      params = process_dates(params)
      raise Error, "Please provide an ID" unless id
      @master.call "/#{@endpoint}/id/#{id}", "put", params
    end

    def destroy(params)
      id = params[:id]
      params = process_dates(params)
      raise Error, "Please provide an ID" unless id
      @master.call "/#{@endpoint}/id/#{id}", "delete", params
    end

    private 
      def process_dates(params)
        processed = {}
        params.each do |key,value|
          if ["start","end","checkedin","checkedout","expires"].include?(key.to_s)
            begin 
              date = DateTime.parse(value.to_s)
              processed[key] = date.strftime("%Y-%m-%d %H:%M:%S")
            rescue
              raise Error, "Please enter a valid date for #{key}"
            end
          else
            processed[key] = value 
          end
        end
        return processed
      end

      def serialize_params(params)
        case "#{params.class}"
        when "Hash"
          params.sort.map{|key, value| "#{key}/#{serialize_params(value)}"}.join("/")
        when 'Array'
          params.join ';;'
        else
          URI::encode(params)
        end
      end
  end

  class Providers < StandardCalls
    attr_accessor :master
    
    def initialize(master)
      @master = master
      super(@master, "provider")
    end
  end

  class Bookables < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "bookable")
    end
  end

  class Resources < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "resource")
    end
  end

  class Blackouts < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "blackout")
    end
  end

  class Timeslots < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
    end

    def search(params)
      params = process_dates(params)
      search = serialize_params(params)
      puts "call /availability/#{search}/"
      @master.call "/availability/#{search}/", "get", params
    end
  end

  class Reservations < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "reservation")
    end
  end

  class Users < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "user")
    end
  end

  class BookableTags < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "bookabletag")
    end
  end

  class RequirementGroups < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "requirementgroup")
    end
  end

  class BookableQuestions < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "bookablequestion")
    end
  end

  class Tickets < StandardCalls
    attr_accessor :master

    def initialize(master)
      @master = master
      super(@master, "ticket")
    end
  end
end